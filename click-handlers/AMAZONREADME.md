For this activity, you will be creating an interactive web page that gets updated or modified when various buttons are clicked. 

 - Begin by creating a project with the following file structure
 
 
```
js_click_handlers/
├── index.html
└── assets/
    ├── javascript/
    │    └──clickhandler.js
    └── stylesheets/
        └── style.css
```

 - Open that project in VS Code.
 - Add the following code to `index.html`:
 
```html
<!DOCTYPE html>
<html>

<head>
    <title>An interactive web page</title>
    <link rel="stylesheet" href="./assets/stylesheets/style.css">
</head>

<body>
    <h1>
        An interactive web page
    </h1>

    <section>
    
        <h2>
            1. Color changer
        </h2>

        <button id="gray-button">
            Gray
        </button>

        <button id="blue-button">
            Blue
        </button>

        <!--
            TODO 1

            Add the HTML for a pink button
        -->

        <div id="color-box" class="gray">
        </div>
        
    </section>

    <hr>
    
    <section>
    
        <h2>
            2. Counter
        </h2>
        
        <button id="counter-button">
            This fabulous button
        </button>
        
        has been clicked
        
        <span id="countspan">
            0
        </span>
        
        times.
        
    </section>

    <hr>
    
    <section>
    
        <h2>
            3. Even or Odd?
        </h2>
        
        <button id="even-or-odd-button">
            Is the count even or odd?
        </button>
        
    </section>


    <script src="./assets/javascript/clickhandler.js"></script>
</body>
</html>
```

- Add the following to `style.css`:

```css
#color-box {
    width: 200px;
    height: 200px;
}
    
.gray {
    background: gray;
}
    
.blue {
    background: rgb(67, 111, 255);
}
    
/* TODO 1
*
* Add a CSS rule for the pink button.
*
*/
```

- Add the following to  `clickhandler.js`:

```js
const colorBox = document.querySelector("#color-box");
    
// JS for the gray button
const grayButton = document.querySelector("#gray-button");
grayButton.addEventListener("click", function () {
    colorBox.className = "gray";
})
    
// JS for the blue button
const blueButton = document.querySelector("#blue-button");
blueButton.addEventListener("click", function () {
    colorBox.className = "blue";
})
    
/* TODO 1
*
* Add the JS for the pink button
*
*/
    
    
let count = 0;
const countspan = document.querySelector("#countspan");
const counterButton = document.querySelector("#counter-button");
counterButton.addEventListener("click", function () {
    /* TODO 2
    *
    * Fill in this function so that it increments
    * (adds one to) the variable named count,
    * then updates the inner text of "countspan"
    * to show the current value of "count".
    */
})
    
const evenOrOddButton = document.querySelector("#even-or-odd-button");
evenOrOddButton.addEventListener("click", function () {
    /* TODO 3
    *
    * Fill in this function so that it shows an
    * alert dialog stating whether the count variable
    * (from part 2, above) is even or odd.
    */
})
```

If you open that `index.html` file in your web browser, you will see three numbered sections.

Clicking on the "blue" and "gray" buttons in section 1 will change the color of the square. The other buttons on the page don't do anything ...yet.

You will need to add HTML, CSS, and JS to make these buttons work.

Comments have been added to point you in the right direction.

* * *

![Edwin_the_duck.jpg](https://i.snag.gy/xZgaDe.jpg)

**Edwin the Duck Says:**
_Even for small projects, its a good idea to use best practices and separate your HTML, CSS, and JS files. This will become even more important in larger projects._

* * *

### Math Hint ###

The third TODO will require you to figure out whether a number is even or odd.

To determine this, you will use JavaScript's modulus, or remainder, operator, `%`.

The modulus operator gives you the [remainder left over when one number is divided by another](https://www.mathsisfun.com/numbers/division-remainder.html).

In arithmetic, a number can be checked even or odd by checking the remainder of the division of the number by 2. If the remainder is 1, then the number is odd. If the remainder is 0, then the number is even.

-	`3 % 2` = 1 _(hence 3 is odd)_
-	`4 % 2` = 0 _(hence 4 is even)_

### Programming hint ###

Make sure you understand the difference between JavaScript's assignment operator (which updates the value of a variable) and the equality operator (which returns true or false depending on whether two values are the same or not).

### Finishing up ###

Once you have completed all three TODOs, your index.html file should display five buttons, and clicking on any of those 
buttons should produce the desired behavior. Push your code into a GitHub repository and share it with your instructor/facilitator.

